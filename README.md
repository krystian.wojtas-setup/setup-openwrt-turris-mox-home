# Purpose

Install extra software and configure [turris mox](https://www.turris.com/en/mox/overview/) router.

# Configuration

Configure password store and fill secrets
``` sh
% pass show openwrt
openwrt
├── acme
│   ├── dynu_client_id
│   └── dynu_secret
├── acme
├── domain_
├── domain
├── passwd
├── uddns
│   ├── password
│   └── user
├── uddns
├── website
├── wireless
│   ├── mac
│   ├── password
│   └── ssid
└── wireless
```

# Install

Run once on fresh os command which:
- upload your ssh public key
- change root password
- set custom ip address
``` sh
./install_once
```

Then restart network configuration on your host to get new ip address in proper network

Then apply rest of installation
``` sh
./install
```

To apply any further changes in repo run only last `./install` command.
